package ch07;

public class Assertions {

    void noReturn() {}
    int aReturn (){return 1;}

    void go() {
        int x=1;
        boolean b=true;

        assert(x==1);
        assert(x==1) : x;
        assert(x==2) : x;

    }

    public static void main(String[] args){
        Assertions a1 = new Assertions();
        a1.go();
    }


}

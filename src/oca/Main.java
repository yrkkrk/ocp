package oca;

/**
 * Created by developer on 17.1.2017.
 */
public class Main {
    static int a = 9;
    static String str = "initial";
    static Integer i = 99;

    public static void main(String[] args){
        MyObject obj = new MyObject();
        MyObject refObj = new MyObject();
        refObj.setA(555);

        System.out.println("Values before trySetting: " + a + " " + str + " " + i + " " + refObj.getA());
        obj.trySetting(a, str, i, refObj);
        System.out.println("Values after trySetting: " + a + " " +str+" "+i+" "+refObj.getA());

        Baba b1 = new Baba();
        Baba b2 = new Ogul();
        Ogul o1 = new Ogul();

        ((Ogul)b2).method2();
    }

}

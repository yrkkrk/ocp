package oca;

/**
 * Created by developer on 17.1.2017.
 */
public class MyObject {
    private int a;
    private String str;
    private Integer i;

    public void trySetting(int a, String str, Integer i, MyObject o){
        System.out.println("Values: " + a + " " +str+" "+i+" "+o.getA());
        a=10; str="changed"; i= new Integer(100);
        //o = new MyObject();
        o.setA(999);
        System.out.println("Values after assign: " + a + " " +str+" "+i+" "+o.getA());
    }

    public int getA() {
        return a;
    }

    public void setA(int a) {
        this.a = a;
    }

    public String getStr() {
        return str;
    }

    public void setStr(String str) {
        this.str = str;
    }


    public Integer getI() {
        return i;
    }

    public void setI(Integer i) {
        this.i = i;
    }
}

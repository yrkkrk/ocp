package ch13;

/**
 * Created by developer on 21.1.2017.
 */
public class Main {

    public static void main(String[] args) {
        Timer timer = new Timer();
        Thread t1 = new Thread(timer);
        Thread t2 = new Thread(timer);
        t1.start();
        t2.start();
    }
}


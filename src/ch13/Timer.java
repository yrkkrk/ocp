package ch13;

/**
 * Created by developer on 21.1.2017.
 */
public class Timer implements Runnable{
    @Override
    public void run() {
        for(int i=0; i<100; i++){
            System.out.print(" "+i);
            if(i%10 == 0){
                System.out.print("Dalya");
            }
            try {
                Thread.currentThread().sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
